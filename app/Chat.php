<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * App\Chat
 *
 * @property int $id
 * @property int $from_id
 * @property int $to_id
 * @property string $message
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Chat newModelQuery()
 * @method static Builder|Chat newQuery()
 * @method static Builder|Chat query()
 * @method static Builder|Chat whereCreatedAt($value)
 * @method static Builder|Chat whereFromId($value)
 * @method static Builder|Chat whereId($value)
 * @method static Builder|Chat whereMessage($value)
 * @method static Builder|Chat whereToId($value)
 * @method static Builder|Chat whereUpdatedAt($value)
 * @mixin Eloquent
 */
class Chat extends Model
{
    protected $guarded = [];

    public static function getLatestChats()
    {
        return DB::select('
        select t.* from chats t
        where not exists (
        select 1 from chats
        where created_at > t.created_at and 
        ((from_id = t.from_id and to_id = t.to_id) or (from_id = t.to_id and to_id = t.from_id))) order by created_at;');
    }
    public static function getLatestChatsOfMineWhichOthersAreTheLatest($userId){
        return DB::select('
        select t.*,pdate(CONVERT_TZ(created_at,"+00:00","'.env('timeOffset').'")) as time from chats t
        where to_id = :to_id and not exists (
        select 1 from chats
        where created_at > t.created_at and 
        ((from_id = t.from_id and to_id = t.to_id) or (from_id = t.to_id and to_id = t.from_id))) order by created_at;',['to_id'=>$userId]);
    }
    public static function getLatestChatsOfMineWhichImAlsoTheLatest($userId){
        return DB::select('
        select t.* from chats t
        where (from_id = :from_id or to_id = :to_id) and not exists (
        select 1 from chats
        where created_at > t.created_at and 
        ((from_id = t.from_id and to_id = t.to_id) or (from_id = t.to_id and to_id = t.from_id))) order by created_at;',['from_id'=>$userId,'to_id'=>$userId]);
    }
    public static function getMessagesOfChat($user1, $user2, $skip = 0, $take = 30){
        return self::where([['from_id',$user2],['to_id',$user1]])->orWhere([['from_id',$user1],['to_id',$user2]])->skip($skip)->take($take)->orderBy('created_at','asc')->selectRaw('*,pdate(CONVERT_TZ(updated_at,"+00:00","'.env('timeOffset').'")) as time')->get();
    }
}
