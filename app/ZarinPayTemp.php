<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ZarinPayTemp
 *
 * @property int $id
 * @property mixed $json
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ZarinPayTemp newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ZarinPayTemp newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ZarinPayTemp query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ZarinPayTemp whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ZarinPayTemp whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ZarinPayTemp whereJson($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ZarinPayTemp whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ZarinPayTemp extends Model
{
    protected $guarded = [];
}
