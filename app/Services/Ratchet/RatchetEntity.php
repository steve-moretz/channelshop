<?php
namespace App\Services\Ratchet;
class RatchetEntity{
    public static function sendMessage($toUserId,$payload,$persist=false){
        return json_encode(['command'=>'message','to'=>$toUserId,'persist'=>$persist] + $payload);
    }
    public static function sendToTopic($topic,$payload,$persist=false){
        return json_encode(['command'=>'message','topic'=>$topic,'persist'=>$persist]+$payload);
    }
    public static function sendCustom($command,$payload){
        return json_encode(['command'=>$command]+$payload);
    }
}
