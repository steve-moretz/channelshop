<?php

namespace App;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

/**
 * App\ExchangeEntity
 *
 * @property string $unitNamePersian
 * @property string $unitNameTurkish
 * @property int $id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExchangeEntity newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExchangeEntity newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExchangeEntity query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExchangeEntity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExchangeEntity whereUnitNamePersian($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExchangeEntity whereUnitNameTurkish($value)
 * @mixin \Eloquent
 * @property int $price
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ExchangeEntity wherePrice($value)
 */
class ExchangeEntity extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();
        static::deleted(static function(ExchangeEntity $exchangeEntity){
            try{
                File::delete('img/exchange_pics/'.($exchangeEntity->id).'.png');
            }catch (Exception $exception){}
        });
    }

}
