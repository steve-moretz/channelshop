<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Animation
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property int $views
 * @property string $picUrl
 * @property string $videoUrl
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Animation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Animation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Animation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Animation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Animation whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Animation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Animation wherePicUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Animation whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Animation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Animation whereVideoUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Animation whereViews($value)
 * @mixin \Eloquent
 * @property string|null $picFile
 * @property string|null $videoFile
 * @property string $videoAspectRatio
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Animation wherePicFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Animation whereVideoAspectRatio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Animation whereVideoFile($value)
 * @property string|null $mimeType
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Animation whereMimeType($value)
 */
class Animation extends Model
{
    protected $guarded = [];
    protected static function boot()
    {
        parent::boot();
        static::deleted(function ($animation){
            $videoPath = "animation-assets/vids/".$animation->videoFile;
            $imagePath = "animation-assets/pics/".$animation->picFile;
            if(\File::exists($videoPath)){\File::delete($videoPath);}
            if(\File::exists($imagePath)){\File::delete($imagePath);}
        });
    }
    public function addViewerCount(){
        $this->views = $this->views;
        $this->update();
    }
}
