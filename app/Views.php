<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Views
 *
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Views newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Views newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Views query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Views whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Views whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Views extends Model
{
    protected $guarded = [];
    public static function addVisitor(){
        self::create();
    }
}
