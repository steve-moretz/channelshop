<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\RatchetOnlineUser
 *
 * @property int $conn_id
 * @property int $user_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RatchetOnlineUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RatchetOnlineUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RatchetOnlineUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RatchetOnlineUser whereConnId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RatchetOnlineUser whereUserId($value)
 * @mixin \Eloquent
 */
class RatchetOnlineUser extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public static function findByConnectionId($conn_id){
        return static::where('conn_id',$conn_id);
    }
    public static function findByUserId($user_id){
        return static::where('user_id',$user_id);
    }
}
