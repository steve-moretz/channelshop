<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

/**
 * App\Order
 *
 * @property int $id
 * @property float $price
 * @property float $from_price
 * @property int $user_id
 * @property int $from_unit
 * @property int $to_unit
 * @property int $confirmed
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereConfirmed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereFromPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereFromUnit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereToUnit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereUserId($value)
 * @mixin \Eloquent
 * @property-read \App\ExchangeEntity $fromExchange
 * @property-read \App\ExchangeEntity $toExchange
 */
class Order extends Model
{
    protected $guarded = [];
    public static function getNotConfirmedsList(){
        return static::where('confirmed',false)->orderBy('created_at','desc')->selectRaw('*,pdate(CONVERT_TZ(created_at,"+00:00","'.env('timeOffset').'")) as time')->get();
    }

    public static function getAllForUserId($userId){
        return static::where('user_id',$userId)->selectRaw('*,pdate(CONVERT_TZ(created_at,"+00:00","'.env('timeOffset').'")) as time')->orderBy('created_at','desc');
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function fromExchange(){
        return $this->hasOne(ExchangeEntity::class,'id','from_unit');
    }

    public function toExchange(){
        return $this->hasOne(ExchangeEntity::class,'id','to_unit');
    }
}
