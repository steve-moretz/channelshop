<?php

namespace App\Http\Controllers;

use App\Comment;
use Auth;
use Illuminate\Routing\Controller;
use function request;

class CommentsController extends Controller
{
    public function create()
    {//User MiddleWare
        $validated = request()->validate([
                'comment' => 'required|string'
            ] + (
                Auth::user()->isAdmin() ? ['username' => 'required|string'] : []
            ));
        Comment::create([
                'comment' => $validated['comment'],
                'user_id' => Auth::user()->id,
                'has_confirmed' => Auth::user()->isAdmin()
            ] + (
                Auth::user()->isAdmin() ? ['username' => $validated['username']] : []
            ));
        return json_encode(['status' => 'ok']);
    }

    public function confirm(Comment $comment)
    {//IsAdmin MiddleWare
        $comment->update(['has_confirmed' => true]);
        return json_encode(['status' => 'ok']);
    }

    public function destroy(Comment $comment)
    {
        try {
            $comment->delete();
        } catch (\Exception $e) {
        }
        return json_encode(['status' => 'ok']);
    }

    public function indexUser()
    {
        return Comment::getIndexForUser();
    }

    public function indexAdmin()
    {
        return Comment::getNotConfirmed();
    }
}
