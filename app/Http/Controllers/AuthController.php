<?php

namespace App\Http\Controllers;

use App\Notifications\SignupActivate;
use App\SmsCode;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class AuthController extends Controller
{

    public function sendcode(){
        SmsCode::removeExpiredCodes();
        $validated = \request()->validate([
            'phone'=>'required|string'
        ]);
        SmsCode::where('phone',$validated['phone'])->delete();
        $code = Str::random(3) . ' ' . Str::random(3);
        SmsCode::create([
            'phone'=>$validated['phone'],
            'code'=> $code
        ]);

        //send code however you want here $code

        //

        return ['status' => 'ok'];
    }
    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function signup(Request $request)
    {
        $registerViaPhone = env('REGISTER_VIA_PHONE', false);
        $request->validate([
            'name' => 'required|string',
            'password' => 'required|string|confirmed',
            $registerViaPhone ? 'phone' : 'email' => $registerViaPhone ? 'required|string|unique:users' : 'required|string|email|unique:users'
        ]+(env('INCLUDE_EMAIL') ? ['email' => 'required|string|email|unique:users'] : [] )+(env('INCLUDE_PHONE') ? ['phone' => 'required|string|unique:users'] : [] )+
            //add extra field validation here
            [

            ]);
        $user = new User([
            'name' => $request->name,
//            'email' => $request->email,
            'password' => bcrypt($request->password),
//            'phone' => $request->phone,//TODO can comment out
            $registerViaPhone ? 'phone' : 'email' => $registerViaPhone ? $request->phone : $request->email,
            'activation_token' => str_random(60)
        ]);
        if(env('INCLUDE_EMAIL')){
            $user->email = $request->email;
        }
        if(env('INCLUDE_PHONE')){
            $user->phone = $request->phone;
        }

        if(env('EMAIL_VALIDATION')){
            $user->notify(new SignupActivate($user));
        }

        if(env('REGISTER_VIA_PHONE')){
        }

        //add extra fields here
        //till here

        $user->save();
        return response()->json([
            'message' => 'Successfully created user!'
        ], 201);
    }

    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $registerViaPhone = env('REGISTER_VIA_PHONE', false);
        $request->validate([
//            'email' => 'required|string|email',//TODO enable
            'password' => 'required|string',
            'remember_me' => 'boolean',
//            'phone' => 'required'//TODO disable
            $registerViaPhone ? 'phone' : 'email' => $registerViaPhone ? 'required' : 'required|string|email'
        ]);
        $credentials = request([$registerViaPhone ? 'phone' : 'email', 'password']);
        if (!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'ws_token' => $tokenResult->token->id,
            'user_id' => $user->id,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }

    public function signupActivate($token)
    {
        $user = User::where('activation_token', $token)->first();
        if (!$user) {
            return response()->json([
                'message' => 'This activation token is invalid.'
            ], 404);
        }
        $user->email_verified_at = Carbon::now();
        $user->activation_token = null;
        $user->save();
        return \Redirect::to(env('REDIRECT_AFTER_VERIFICATION'));
    }
}
