<?php

namespace App\Http\Controllers;

use App\Testimontional;
use Illuminate\Routing\Controller;

class TestimontionalController extends Controller
{
    public function create(){
        $validated = \request()->validate([
            'title'=>'string|required',
            'description'=>'string|required',
        ]);
        Testimontional::create($validated);
        return json_encode(['status' => 'ok']);
    }

    public function update(Testimontional $testimontional){
        $validated = \request()->validate([
            'title'=>'string|required',
            'description'=>'string|required',
        ]);
        $testimontional->update($validated);
        return json_encode(['status' => 'ok']);
    }

    public function destroy(Testimontional $testimontional){
        try {
            $testimontional->delete();
        } catch (\Exception $e) {
        }
        return json_encode(['status' => 'ok']);
    }

    public function indexUser(){
        return Testimontional::take(5)->orderBy('updated_at','desc')->get(['title','description']);
    }
    public function indexAdmin(){
        return Testimontional::take(5)->orderBy('updated_at','desc')->get(['id','title','description']);
    }
}
