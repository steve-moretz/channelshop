<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Routing\Controller;

class SubscriptionController extends Controller
{
    private function updateExpired(){
        User::whereDate('subscription_expiration','<',Carbon::now())->update(['checked'=>false]);
    }
    public function getNotChecked(){
        $this->updateExpired();
        $collection = User::where('checked', false)->get(['subscription_expiration', 'name','id','phone','subscription_length']);
        foreach ($collection as $item){
            $item->gotSubscription = $item->hasValidSubscription();
        }
        return $collection;
    }
    public function check(User $user){
        $update = ['checked'=>true];
        if(!$user->hasValidSubscription()){
            $update += ['subscription_expiration'=>null];
            $update += ['subscription_length',null];
        }
        $user->update($update);
        return json_encode(['status'=>'ok']);
    }
}
