<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Shopping
 *
 * @property int $id
 * @property int $user_id
 * @property int $price
 * @property int $plan_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Shopping newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Shopping newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Shopping query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Shopping whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Shopping whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Shopping wherePlanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Shopping wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Shopping whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Shopping whereUserId($value)
 * @mixin \Eloquent
 */
class Shopping extends Model
{
    protected $guarded = [];
}
