import PropTypes from 'prop-types'
import React, {useEffect, useState} from 'react';
import moment from "moment/moment";

const NotificationDropdownItem = props => {
    const [newTime, setNewTime] = useState(moment(props.time).fromNow());
    useEffect(()=>{
        let intervalId = setInterval(()=>{
            setNewTime(moment(props.time).fromNow());
        },5000);
        return ()=>{
            clearInterval(intervalId);
        }
    },[]);
    return (
        <div>
            <a href={props.link} className="dropdown-item">
                <i className={`fa ml-2 ${props.icon}`}></i>{props.title}
                <span className="float-left text-muted text-sm">{newTime}</span>
            </a>
            <div className="dropdown-divider"></div>
        </div>
    )
};

export default NotificationDropdownItem;

NotificationDropdownItem.propTypes = {
  icon: PropTypes.any,
  link: PropTypes.any,
  time: PropTypes.any,
  title: PropTypes.any
}
