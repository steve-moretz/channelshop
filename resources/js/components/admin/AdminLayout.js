import React from 'react';
import {Env} from "../../env";

const AdminLayout = props => {
    return (
        <div className="wrapper">
            {/* Navbar */}
            <nav className="main-header navbar navbar-expand bg-white navbar-light border-bottom">
                {/* Left navbar links */}
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <a className="nav-link" data-widget="pushmenu" href="#"><i className="fa fa-bars"></i></a>
                    </li>
                    <li className="nav-item d-none d-sm-inline-block">
                        <a href="/admin-panel" className="nav-link">خانه</a>
                    </li>
                </ul>

                {/* SEARCH FORM */}
                {
                    !!props.payload.search && (
                        <form className="form-inline ml-3">
                            <div className="input-group input-group-sm">
                                <input className="form-control form-control-navbar" type="search" placeholder="جستجو"
                                       aria-label="Search"/>
                                <div className="input-group-append">
                                    <button className="btn btn-navbar" type="submit">
                                        <i className="fa fa-search"/>
                                    </button>
                                </div>
                            </div>
                        </form>
                    )
                }

                {/* Right navbar links */}
                <ul className="navbar-nav mr-auto">
                    {/* Messages Dropdown Menu */}
                    {props.payload.dropdown}
                    {/* Notifications Dropdown Menu */}
                </ul>
            </nav>
            {/* /.navbar */}

            {/* Main Sidebar Container */}
            <aside className="main-sidebar sidebar-dark-primary elevation-4">
                {/* Brand Logo */}
                <a href="/admin-panel" className="brand-link">
                    <span className="brand-text font-weight-light">پنل مدیریت</span>
                </a>

                {/* Sidebar */}
                <div className="sidebar">
                    <div>
                        {/* Sidebar user panel (optional) */}
                        <div className="user-panel mt-3 pb-3 mb-3 d-flex">
                            <div className="image">
                                <img src="/admin/dist/img/admin.jpg" className="img-circle elevation-2"
                                     alt="User Image"></img>
                            </div>
                            <div className="info">
                                <a href="#" className="d-block">{props.payload.adminName}</a>
                            </div>
                        </div>

                        {/* Sidebar Menu */}
                        <nav className="mt-2">
                            <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                                data-accordion="false">
                                {/* Add icons to the links using the .nav-icon class with font-awesome or any other icon font library */}
                                {props.nav && props.nav.map((item)=>{
                                    let navUrl = Env.domain + props.basename + (item.path === '/' ? '' : item.path);
                                    return (
                                        <li key={item.path} className="nav-item">
                                            <a href={navUrl}
                                               className={`nav-link ${navUrl !== props.basename && (location.protocol + '//' + location.host + location.pathname) === (navUrl) ? 'active' : ''}`}>
                                                <i className={`nav-icon fa ${item.payload && item.payload.icon ? item.payload.icon : 'fa-th'}`}></i>
                                                <p>
                                                    {item.payload && item.payload.badge && <span className={`right badge badge-${item.payload && item.payload.badgeColor ? item.payload.badgeColor : 'danger'}`}>{item.payload.badge}</span>}
                                                    {item.payload && item.payload.title}
                                                </p>
                                            </a>
                                        </li>
                                    )
                                })}
                            </ul>
                        </nav>
                        {/* /.sidebar-menu */}
                    </div>
                </div>
                {/* /.sidebar */}
            </aside>

            {/* Content Wrapper. Contains page content */}
            <div className="content-wrapper">
                {/* Content Header (Page header) */}
                <div className="content-header">
                    <div className="container-fluid">
                        <div className="row mb-2">
                            <div className="col-sm-6">
                                <h1 className="m-0 text-dark">{props.payload.title}</h1>
                                <h5 className="mt-2">{props.payload.subtitle}</h5>
                            </div>
                            {/* /.col */}
                            <div className="col-sm-6">
                            </div>
                            {/* /.col */}
                        </div>
                        {/* /.row */}
                    </div>
                    {/* /.container-fluid */}
                </div>
                {/* /.content-header */}

                {/* Main content */}
                <div className="content">
                    <div className="container-fluid">
                        {props.payload.content && props.payload.content}
                        {/* /.row */}
                    </div>
                    {/* /.container-fluid */}
                </div>
                {/* /.content */}
            </div>
            {/* /.content-wrapper */}

            {/* Control Sidebar */}

            {/* /.control-sidebar */}

            {/* Main Footer */}
            <footer className="main-footer">
                {/* To the right */}
                <div className="float-right d-none d-sm-inline">
                    Anything you want
                </div>
                {/* Default to the left */}
                <strong>CopyRight &copy; 2019 <a href="whatsapp://send?abid=+77077924927&text=Hello%2C%20World!">سیاوش
                    مبرهن</a>.</strong>
            </footer>
        </div>
    )
};

export default AdminLayout;
