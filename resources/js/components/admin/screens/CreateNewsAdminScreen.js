import React, {useEffect, useRef, useState} from 'react';
import {CardContent, makeStyles} from "@material-ui/core";
import {Editor} from 'react-draft-wysiwyg';
import {ContentState, convertToRaw, EditorState} from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import Card from "@material-ui/core/Card";
import Typography from "@material-ui/core/Typography";
import Form from "../../../modules/Form";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import UploaderComponent from "../../../modules/UploaderComponent";
import {Env} from "../../../env";
import ReactImageFallback from "react-image-fallback";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";

const useStyles = makeStyles(theme => ({}));

const validate = (name, text) => {
    console.log(name);
    if(name === 'summary'){
        if (text.length > 1000) {
            return 'خیلی طولانی است';
        }
    }else{
        if (text.length > 100) {
            return 'خیلی طولانی است';
        }
    }
    if(text.length === 0){
        return 'این فیلد باید پر شود'
    }
    return true;
};

export default function CSSGrid(props) {
    const isUpdate = !!props.news;
    const classes = useStyles();

    const [editorState, setEditorState] = useState();
    const [html, setHtml] = useState();
    const [isEnglish, setIsEnglish] = useState(0);


    const [updatedPicture, setUpdatedPicture] = useState(new Date().getUTCMilliseconds());

    const form = useRef();

    useEffect(() => {
        convertHtmlTodraft(props.news && props.news.html);
        setIsEnglish(props.news && props.news.isEnglish)
    }, []);



    const convertHtmlTodraft = (html) => {
        if(!html)return;
        const contentBlock = htmlToDraft(html);
        if (contentBlock) {
            const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
            const editorState = EditorState.createWithContent(contentState);
            setEditorState(editorState);
        }
    };

    const isEditorEmpty = () => {
        return html.length === 8 && html.indexOf('<p></p>') ===0;
    };

    return (<div>
        <Card className="p-3">
            <Form ref={form} className="text-center" notValidateOnStart onFetchInit={() => console.log('init')} onFetchEnd={(json) => {
                if(json.status === 'ok'){
                    if(!isUpdate){
                        window.location.href = `update/${json.id}`;
                    }else{
                        window.location.href = '../';
                    }
                }
            }} validate={validate} url={isUpdate ? `admin-panel/news/update/${props.news.id}` : 'admin-panel/news/create'}
                  init={{method: 'POST',body:{html : html,isEnglish : isEnglish}}}>
                <CardContent>
                    <Grid className={classes.root} container spacing={3} justify="center" direction="row" alignItems="center">
                        {isUpdate && (
                            <Grid xs={12} md={12} item>
                                <UploaderComponent className="text-center mb-3 exchange-image" onResponse={(data)=>{
                                    setUpdatedPicture(new Date().getUTCMilliseconds())
                                }} accept="image/*" url={Env.domain + '/admin-panel/news/upload'} body={{id : props.news.id}} onUploadPercent={(percent)=>{}}>
                                    <ReactImageFallback
                                        src={`/img/news/${props.news.id}.png?${updatedPicture}`}
                                        fallbackImage="/img/exchange_pics/default.png"
                                        initialImage="loader.gif"
                                        alt="cool image should be here"
                                        heigth={140}
                                        width={350}/>
                                </UploaderComponent>
                            </Grid>
                        )}
                        <TextField id="outlined-basic" value={props.news && props.news.title} name='title' label="تیتر خبر"/>
                        <TextField className="mr-5"  value={props.news && props.news.link} id="outlined-basic" name='link' label="لینک دسترسی خبر"/>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    defaultChecked
                                    value="secondary"
                                    color="primary"
                                    checked={isEnglish}
                                    onChange={(event)=>{setIsEnglish(event.target.checked)}}
                                    inputProps={{ 'aria-label': 'secondary checkbox' }}/>
                            }
                            label="Is English"
                        />
                        <Grid xs={12} md={10} item>
                            <Card className="mt-5">
                                <CardContent>
                                    <TextField className="w-100" value={props.news && props.news.summary} multiline={true} id="outlined-basic" name='summary' label="خلاصه خبر"/>
                                </CardContent>
                            </Card>
                        </Grid>
                    </Grid>
                </CardContent>
            </Form>
        </Card>
        <Card className="mt-3">
            <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                    متن کامل خبر
                </Typography>
                <Editor
                    editorState={editorState}
                    onEditorStateChange={(data)=>{
                        setEditorState(data);
                        setHtml(draftToHtml(convertToRaw(data.getCurrentContent())));
                        // console.log(draftToHtml(convertToRaw(data.getCurrentContent())));
                    }}/>
            </CardContent>
        </Card>
        <Button className="mt-3" variant="contained" color="primary" onClick={()=>{form.current.submit()}} disabled={html ? (html.length === 8 && html.indexOf('<p></p>') === 0 || !form.current.getData().formIsValid) : true}>
            انتشار
        </Button>
    </div>)
}
