export const ADD_ALL_ORDERS = 'ADD_ALL_ORDERS';
export const REMOVE_ORDER = 'REMOVE_ORDER';

export const removeOrder = (id) => {
    return{
        type: REMOVE_ORDER,
        id
    };
};
export const addAllOrders = (orders) => {
    return{
        type: ADD_ALL_ORDERS,
        orders
    };
};
