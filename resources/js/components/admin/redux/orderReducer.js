import {ADD_ALL_ORDERS, REMOVE_ORDER} from "./orderAction";
import {order} from "@material-ui/system";

const initialState = {
    orders : []
};

export default (state = initialState,action) => {
    switch (action.type) {
        case ADD_ALL_ORDERS:
            return {...state,orders : action.orders};
        case REMOVE_ORDER:
            return {...state,orders : state.orders.filter((item)=>{return item.id != action.id})}
    }
    return state;
}
