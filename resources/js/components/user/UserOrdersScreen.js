import React, {useEffect, useState} from 'react';
import {CardContent, makeStyles, TableFooter} from "@material-ui/core";
import Card from "@material-ui/core/Card";
import TableContainer from "@material-ui/core/TableContainer";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/es/TableCell";
import Paper from "@material-ui/core/Paper";
import TableBody from "@material-ui/core/TableBody";
import TablePagination from '@material-ui/core/TablePagination';
import fetchServer from "../../modules/fetchServer";
import Divider from "@material-ui/core/Divider";
import Grid from "@material-ui/core/Grid";
import {useWidth} from "../../modules/utils";
import IconButton from "@material-ui/core/IconButton";
import PhotoCamera from "@material-ui/icons/esm/PhotoCamera";

const useStyles = makeStyles(theme => ({}));

export default function CSSGrid(props) {
    const classes = useStyles();
    const [orders, setOrders] = useState([]);
    const [count, setCount] = useState(0);
    const [page, setPage] = useState(0);
    const [loadEachPage, setLoadEachPage] = useState(10);
    const width = useWidth();
    useEffect(() => {
        loadPageHandler(1);
    }, []);

    const loadPageHandler = async (page) => {
        const response = await fetchServer(`/orders/${page}/${loadEachPage}`, 'POST');
        const json = await response.json();
        console.log(json);
        setOrders(json.arr);
        setCount(json.count);
        setPage(page)
    };

    useEffect(() => {
        loadPageHandler(0);
    }, [loadEachPage]);


    return (
        <TableContainer component={Paper} style={{
            marginTop: -150,
            marginBottom: 28,
            background: 'white',
            width: '95%',
            marginLeft: 'auto',
            marginRight: 'auto'
        }} elevation={20}>
            <Table className={classes.table} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell>id</TableCell>
                        <TableCell align="center">date</TableCell>
                        <TableCell align="center">from</TableCell>
                        <TableCell align="center">to</TableCell>
                        <TableCell align="center">Status</TableCell>
                        <TableCell align="center">Request Transaction</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {orders && orders.map(order => (
                        <TableRow key={order.id}>
                            <TableCell component="th" scope="row">
                                {order.id}
                            </TableCell>
                            <TableCell align="center">{order.time}</TableCell>
                            <TableCell align="center">
                                <Grid style={{flexDirection: 'row'}} container justify='center' alignItems='center'>
                                    {`${order.from_price.toFixed(2)} ${order.fromUnitNamePersian}`}
                                    {width !== 'xs' && (
                                        <Divider orientation='vertical'
                                                 style={{height: 50, marginRight: 10, marginLeft: 10}}/>
                                    )}
                                    <img src={'/img/exchange_pics/' + order.from_unit + '.png'} width={30} height={30}/>
                                </Grid>
                            </TableCell>
                            <TableCell align="center">
                                <Grid style={{flexDirection: 'row'}} container justify='center' alignItems='center'>
                                    {`${order.price.toFixed(2)} ${order.toUnitNamePersian}`} {width !== 'xs' && (
                                    <Divider orientation='vertical'
                                             style={{height: 50, marginRight: 10, marginLeft: 10}}/>
                                )}
                                    <img src={'/img/exchange_pics/' + order.to_unit + '.png'} width={30} height={30}/>
                                </Grid>
                            </TableCell>
                            <TableCell align="center">{order.confirmed === 1 ? (
                                <div>
                                    <h6>Done</h6>
                                    <IconButton color="primary" aria-label="upload picture" component="span" onClick={()=>{
                                        window.open(`/img/orders/${order.id}-confirmed.png`)
                                    }}>
                                        <PhotoCamera />
                                    </IconButton>
                                </div>
                            ):(
                                'In Progress...'
                            )}</TableCell>
                            <TableCell align="center">
                                <IconButton color="primary" aria-label="upload picture" component="span" onClick={()=>{
                                    window.open(`/img/orders/${order.id}-request.png`)
                                }}>
                                    <PhotoCamera />
                                </IconButton>
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
                <TableFooter>
                    <TablePagination rowsPerPage={loadEachPage} onChangeRowsPerPage={event => {
                        setLoadEachPage(event.target.value);
                    }} page={page} count={count} onChangePage={(event,page) => {
                        loadPageHandler(page)
                    }}/>
                </TableFooter>
            </Table>
        </TableContainer>
    )
}
