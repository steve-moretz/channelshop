import React, {useEffect, useRef, useState} from 'react';
import {Button, CardContent, makeStyles, TextField, Typography} from "@material-ui/core";
import Card from "@material-ui/core/Card";
import HeaderTitle from "./components/HeaderTitle";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import i18n from 'i18n-js';
import Grid from "@material-ui/core/Grid";
import Divider from "@material-ui/core/Divider";
import moment from "moment-timezone";
import Snackbar from "@material-ui/core/Snackbar";
import UploaderComponent from "../../modules/UploaderComponent";
import CloudUpload from "@material-ui/icons/esm/CloudUpload";
import Colors from "../constants/Colors";
import CircularProgress from "@material-ui/core/CircularProgress";

const useStyles = makeStyles(theme => ({}));

var turkeyNow = moment.tz("Europe/Istanbul");
var turkeyOpen = moment.tz('9:00am', 'h:mma', "Europe/Istanbul");
var turkeyClose = moment.tz('17:00pm', 'h:mma', "Europe/Istanbul");

export default function CSSGrid({table}) {
    const classes = useStyles();
    const [offset, setOffset] = useState(-50);
    const [fromIdSelected, setFromIdSelected] = useState('');
    const [toIdSelected, setToIdSelected] = useState('');

    const [textInput, setTextInput] = useState('');
    const [resText, setResText] = useState();

    const uploader = useRef();

    const [preview, setPreview] = useState();

    const [open, setOpen] = useState(false);

    const [uploading, setUploading] = useState(false);

    useEffect(() => {
        if(fromIdSelected && toIdSelected){
            textChangeHandler(textInput);
        }
    }, [fromIdSelected, toIdSelected]);


    const textChangeHandler = (t) => {
        if(t.length > 10){return;}
        if(!fromIdSelected || !toIdSelected){
            setOpen(true);
            return;
        }
        const from = table.find(item=>{return item.id === fromIdSelected});
        const to = table.find(item=>{return item.id === toIdSelected});
        var text = t.replace(/[^\u06F0-\u06F90-9.]/gi, "");
        setTextInput(text);
        if (from.price === 1) {//It's a sell

        } else if (to.price === 1) {//It's a buy(has the offset)

        } else {//It's a buy and the it's a sell(first has the offset then not)
            // console.log(from);
            // console.log(to);
            var buy = text * (from.price + offset);
            // console.log(buy);
            var sell = buy * (1 / to.price);
            setResText(sell.toFixed(2));
        }
    };

    const onSubmitHandler = async () => {
        const response = await uploader.current.submit();
        console.log(response);
        if(response && response != 'error'){
            window.location.href = '/orders';
        }else{
            alert('Something went wrong!Please Try Again')
        }
    };

    return (
        <Card style={{
            marginTop: -110,
            marginBottom: 28,
            background: 'white',
            width: '95%',
            marginLeft: 'auto',
            marginRight: 'auto'
        }} elevation={20}>
            <Snackbar
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}
                open={open}
                autoHideDuration={6000}
                onClose={()=>{setOpen(false)}}
                message={<span className='text-white'>Please first choose the units</span>}/>
            <CardContent style={{alignItems: 'center', justifyContent: 'center'}}>
                <HeaderTitle title='Create A New Order'/>
                <Grid container style={{flexDirection: 'column'}} justify='center' alignItems='center' className='text-center'>
                    <Card elevation={10}>
                        <CardContent style={{alignItems: 'center', justifyContent: 'center'}}>
                            <Grid item>
                                <TextField value={textInput} variant='outlined'  type="number" onChange={(event)=>{
                                    textChangeHandler(event.target.value)
                                }} style={{maxWidth: 130, marginRight: 15,textAlign : 'center'}}/>
                                <FormControl style={{minWidth: 110}}>
                                    <InputLabel id="demo-simple-select-label">From</InputLabel>
                                    <Select
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        value={fromIdSelected}
                                        onChange={(toId) => {
                                            setFromIdSelected(toId.target.value);
                                        }}>
                                        {table.map((item) => {
                                            return <MenuItem value={item.id}>{item[i18n.t('unitFieldName')]}</MenuItem>;
                                        })}
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Divider className='mt-3 mb-3'/>
                            <Grid container style={{flexDirection : 'row'}} alignItems='center' justify='center'>
                                <Grid item style={{minWidth : 130,marginRight: 15}}>
                                    <Typography variant='body1' component='p' style={{maxWidth: 130}}>{resText}</Typography>
                                </Grid>
                                    <FormControl style={{minWidth: 110}}>
                                        <InputLabel id="demo-simple-select-label">To</InputLabel>
                                        <Select
                                            labelId="demo-simple-select-label"
                                            id="demo-simple-select"
                                            value={toIdSelected}
                                            onChange={(toId) => {
                                                setToIdSelected(toId.target.value);
                                            }}>
                                            {table.map((item) => {
                                                return <MenuItem value={item.id}>{item[i18n.t('unitFieldName')]}</MenuItem>;
                                            })}
                                        </Select>
                                    </FormControl>
                            </Grid>
                        </CardContent>
                    </Card>
                </Grid>
                <Divider className='mt-3 mb-3'/>
                <HeaderTitle title='Upload Transaction Picture'/>
                <Grid container alignItems='center' style={{flexDirection : 'column'}}>
                    <UploaderComponent ref={uploader} body={{
                        fromUnitId: fromIdSelected,
                        toUnitId: toIdSelected,
                        amount: textInput
                    }} url='/order/create' noAutoUpload={true} onUploadPercent={(percent)=>{
                        setUploading(percent < 100);
                    }} onPictureSrcSelected={setPreview} className='text-center cursor-pointer uploader'>
                        <Card elevation={10}>
                            <CardContent>
                                {
                                    preview ? (
                                        <img src={preview} className='img-fluid'/>
                                    ):(
                                        <div>
                                            <CloudUpload size={32} style={{ fontSize: 40 ,elevation : 30}} elevation={10}/>
                                        </div>
                                    )
                                }
                            </CardContent>
                        </Card>
                    </UploaderComponent>
                    {
                        uploading ? (
                            <CircularProgress style={{marginTop : 10}}/>
                        ):(
                            toIdSelected && fromIdSelected && textInput && preview && (
                                <Button variant='contained' color="primary" style={{elevation : 10,marginTop : 10}} onClick={onSubmitHandler}>Submit</Button>
                            )
                        )
                    }
                </Grid>
            </CardContent>
        </Card>
    )
}
