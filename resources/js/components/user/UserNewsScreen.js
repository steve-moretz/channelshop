import React from 'react';
import {CardContent, makeStyles} from "@material-ui/core";
import Card from "@material-ui/core/Card";
import HeaderTitle from "./components/HeaderTitle";
import CardMedia from "@material-ui/core/CardMedia";
import CardActionArea from "@material-ui/core/CardActionArea";
import NewsComponent from "./NewsComponent";
import {isLangPersian} from "./UserLayout";

const useStyles = makeStyles(theme => ({
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
}));

export default function CSSGrid({detail}) {
    const classes = useStyles();
    return (
        <NewsComponent style={{marginTop: -150,marginBottom : 28, background: 'white', width: '95%', marginLeft: 'auto', marginRight: 'auto'}} title={detail.title}  date={isLangPersian() ? detail.time : detail.updated_at} image={`/img/news/${detail.id}.png`} link='test' summary={detail.summary} html={detail.html}/>
     )
}
