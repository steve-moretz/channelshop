import PropTypes from 'prop-types'
import React from 'react';

const HeaderTitle = (props) => {
    return <h1 style={{textAlign : 'center',...props.style}}>{props.title}</h1>
};
export default HeaderTitle;

HeaderTitle.propTypes = {
  style: PropTypes.any,
  title: PropTypes.any
}
