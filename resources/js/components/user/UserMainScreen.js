import PropTypes from 'prop-types'
import React, {useEffect, useRef, useState} from 'react';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import i18n from 'i18n-js';
import moment from "moment-timezone";
import Card from "@material-ui/core/Card";
import {CardContent} from "@material-ui/core";
import Clock from 'react-clock/dist/entry.nostyle';
import '../../../../public/css/Clock.css';
import Grid from "@material-ui/core/Grid";
import Colors from "../constants/Colors";
import Divider from "@material-ui/core/Divider";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import GridList from "@material-ui/core/GridList";
import NewsComponent from "./NewsComponent";
import DragScrollProvider from 'drag-scroll-provider'
import GridListTile from "@material-ui/core/GridListTile";
import {isLangPersian} from "./UserLayout";
import HeaderTitle from "./components/HeaderTitle";

const useStyles = makeStyles(theme => ({
    table: {
        minWidth: 650,
    },
    gridList: {
        flexWrap: 'nowrap',
        // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
        transform: 'translateZ(0)',
        overflowY : 'hidden',
        height : 520,
        padding : 10,
        scrollbarWidth : 'thin',
        paddingRight : 25,
    },
    title: {
        color: theme.palette.primary.light,
    },
    titleBar: {
        background:
            'linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
    },
}));

const faToEnDigits = function (input) {
    if (input == undefined)
        return;
    var returnModel = "", symbolMap = {
        '۱': '1',
        '۲': '2',
        '۳': '3',
        '۴': '4',
        '۵': '5',
        '۶': '6',
        '۷': '7',
        '۸': '8',
        '۹': '9',
        '۰': '0'
    };
    input = input.toString();
    for (var i = 0; i < input.length; i++)
        if (symbolMap[input[i]])
            returnModel += symbolMap[input[i]];
        else
            returnModel += input[i];
    return returnModel;
};


const ClockTimezoneComponent = (props) => {
    const [date, setDate] = useState(faToEnDigits(moment.tz(props.timezone).format('HH:mm:ss')));
    useEffect(() => {
        var timer = setInterval(() => {
            setDate(faToEnDigits(moment.tz(props.timezone).format('HH:mm:ss')))
        }, 1000);
        return () => {
            clearInterval(timer);
            timer = null;
        }
    }, []);
    return (
        <Card style={{textAlign: 'center', backgroundColor: '#fbfbfb'}} elevation={10}>
            <CardContent>
                <Clock size={100} value={date}/>
                <h6 style={{marginTop: 10}}>{props.name}</h6>
            </CardContent>
        </Card>
    )
};

const StyledTableCell = withStyles(theme => ({
    head: {
        backgroundColor: Colors.primary,
        color: Colors.accent,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const StyledTableRow = withStyles(theme => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: '#e0e0e0',
        },
    },
}))(TableRow);

export default function SimpleTable(props) {
    const classes = useStyles();

    const [offset, setOffset] = useState(-50);

    const sliderRef = useRef();

    var turkeyNow = moment.tz("Europe/Istanbul");
    var turkeyOpen = moment.tz('9:00am', 'h:mma', "Europe/Istanbul");
    var turkeyClose = moment.tz('17:00pm', 'h:mma', "Europe/Istanbul");


    useEffect(() => {
        var timer = setInterval(() => {
            turkeyNow = moment.tz("Europe/Istanbul");
            if (turkeyNow.isBefore(turkeyClose) && turkeyNow.isAfter(turkeyOpen)) {
                if (offset !== -50) {
                    setOffset(-50);
                }
            } else {
                if (offset !== -100) {
                    setOffset(-100);
                }
            }
        }, 1000);
        return () => {
            clearInterval(timer);
            timer = null;
        }
    }, [offset]);
    useEffect(() => {
        // sliderRef.current.scrollTo(50);
    }, []);


    return (
        <Card style={{marginTop: -150,marginBottom : 28, background: 'white', width: '95%', marginLeft: 'auto', marginRight: 'auto'}} elevation={20}>
            <CardContent>
                <HeaderTitle title='Clocks'/>
                <Grid container justify='center'>
                    <Grid style={{marginTop: 10, marginLeft: 5, marginRight: 5}}>
                        <ClockTimezoneComponent timezone='Asia/Tehran' name='Tehran'/>
                    </Grid>
                    <Grid style={{marginTop: 10, marginLeft: 5, marginRight: 5}}>
                        <ClockTimezoneComponent timezone='Europe/Istanbul' name='Istanbul'/>
                    </Grid>
                    <Grid style={{marginTop: 10, marginLeft: 5, marginRight: 5}}>
                        <ClockTimezoneComponent timezone='Europe/Istanbul' name='Istanbul'/>
                    </Grid>
                    <Grid style={{marginTop: 10, marginLeft: 5, marginRight: 5}}>
                        <ClockTimezoneComponent timezone='Europe/Istanbul' name='Istanbul'/>
                    </Grid>
                    <Grid style={{marginTop: 10, marginLeft: 5, marginRight: 5}}>
                        <ClockTimezoneComponent timezone='Europe/Istanbul' name='Istanbul'/>
                    </Grid>
                    <Grid style={{marginTop: 10, marginLeft: 5, marginRight: 5}}>
                        <ClockTimezoneComponent timezone='Europe/Istanbul' name='Istanbul'/>
                    </Grid>
                </Grid>
                <Divider style={{marginTop: 15, marginBottom: 15}}/>
                <HeaderTitle title='Exchange Table'/>
                <TableContainer component={Paper}>
                    <Table className={classes.table} aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <StyledTableCell align="left">{i18n.t('unitPriceBuy')}</StyledTableCell>
                                <StyledTableCell align="left">{i18n.t('unitPrice')}</StyledTableCell>
                                <StyledTableCell align="right">{i18n.t('unitTitle')}</StyledTableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {props.table.map(row => (
                                <StyledTableRow key={row.name}>
                                    <StyledTableCell align="left">{row.price}</StyledTableCell>
                                    <StyledTableCell align="left">{row.price + offset}</StyledTableCell>
                                    <StyledTableCell align="right">{row[i18n.t('unitFieldName')]}</StyledTableCell>
                                </StyledTableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                <Divider style={{marginTop: 15, marginBottom: 15}}/>
                <h1 style={{textAlign : 'center',marginTop : 40}}>News</h1>
                <DragScrollProvider ref={sliderRef}>
                    {({ onMouseDown, ref }) => (
                        <GridList className={classes.gridList} cols={2.5} ref={ref} onMouseDown={onMouseDown}>
                            {
                                props.news.map((item)=>{
                                    console.log(item);
                                    return (
                                        <GridListTile style={{minWidth : 400,maxWidth : 300,minHeight : 500}} key={item.id}>
                                            <div style={{margin : 20,maxWidth : 345}}>
                                                <NewsComponent image={`/img/news/${item.id}.png`} title={item.title} date={isLangPersian() ? item.time : item.updated_at} link={`/news/${item.link}`} summary={item.summary}/>
                                            </div>
                                        </GridListTile>
                                    )
                                })
                            }
                        </GridList>
                    )}
                </DragScrollProvider>
            </CardContent>
        </Card>
    );
}

ClockTimezoneComponent.propTypes = {
    name: PropTypes.any,
    timezone: PropTypes.any
}

HeaderTitle.propTypes = {
  title: PropTypes.any
}
