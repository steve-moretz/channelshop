import React, {useEffect, useRef, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Badge from '@material-ui/core/Badge';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MailIcon from '@material-ui/icons/Mail';
import NotificationsIcon from '@material-ui/icons/Notifications';
import MoreIcon from '@material-ui/icons/MoreVert';
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import ListItemText from "@material-ui/core/ListItemText";
import SwipeableDrawer from "@material-ui/core/SwipeableDrawer";
import Cookies from "universal-cookie";
import Ws from "../../modules/WS";
import ChatWindow, {MessageChatItem} from "../../modules/ChatWindow";
import {useDispatch, useSelector} from "react-redux";
import fetchServer from "../../modules/fetchServer";
import {addAllChatMessages} from "../admin/redux/chatsAction";
import {Env} from "../../env";

const useStyles = makeStyles(theme => ({
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        display: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'block',
        },
    },
    searchIcon: {
        width: theme.spacing(7),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        color: 'inherit',
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 7),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    sectionDesktop: {
        display: 'none',
        [theme.breakpoints.up('md')]: {
            display: 'flex',
        },
    },
    sectionMobile: {
        display: 'flex',
        [theme.breakpoints.up('md')]: {
            display: 'none',
        },
    },
}));

const cookies = new Cookies();

const setLangHandler = (lang) => {
    if(cookies.get('lang') !== 'lang'){
        cookies.set('lang',lang,{path : '/'});
        window.location.reload();
    }
};

export const isLangEnglish = () => {
    const cookies = new Cookies();
    return (cookies.get('lang') === 'en');
};
export const isLangPersian = () => {
    const cookies = new Cookies();
    return (cookies.get('lang') === 'fa');
};

export default function PrimarySearchAppBar(props) {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);

    const isMenuOpen = Boolean(anchorEl);
    const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

    const chatMessages = useSelector(state => state.chats.chatMessages);

    const handleProfileMenuOpen = event => {
        setAnchorEl(event.currentTarget);
    };

    const handleMobileMenuClose = () => {
        setMobileMoreAnchorEl(null);
    };

    const handleMenuClose = () => {
        setAnchorEl(null);
        handleMobileMenuClose();
    };

    const handleMobileMenuOpen = event => {
        setMobileMoreAnchorEl(event.currentTarget);
    };

    const menuId = 'primary-search-account-menu';
    const renderMenu = (
        <Menu
            anchorEl={anchorEl}
            anchorOrigin={{vertical: 'top', horizontal: 'right'}}
            id={menuId}
            keepMounted
            transformOrigin={{vertical: 'top', horizontal: 'right'}}
            open={isMenuOpen}
            onClose={handleMenuClose}
        >
            <MenuItem onClick={handleMenuClose}>Profile</MenuItem>
            <MenuItem onClick={handleMenuClose}>My account</MenuItem>
        </Menu>
    );

    const mobileMenuId = 'primary-search-account-menu-mobile';
    const renderMobileMenu = (
        <Menu
            anchorEl={mobileMoreAnchorEl}
            anchorOrigin={{vertical: 'top', horizontal: 'right'}}
            id={mobileMenuId}
            keepMounted
            transformOrigin={{vertical: 'top', horizontal: 'right'}}
            open={isMobileMenuOpen}
            onClose={handleMobileMenuClose}
        >
            <MenuItem>
                <IconButton aria-label="show 4 new mails" color="inherit">
                    <Badge badgeContent={4} color="secondary">
                        <MailIcon/>
                    </Badge>
                </IconButton>
                <p>Messages</p>
            </MenuItem>
            <MenuItem>
                <IconButton aria-label="show 11 new notifications" color="inherit">
                    <Badge badgeContent={11} color="secondary">
                        <NotificationsIcon/>
                    </Badge>
                </IconButton>
                <p>Notifications</p>
            </MenuItem>
            <MenuItem onClick={handleProfileMenuOpen}>
                <IconButton
                    aria-label="account of current user"
                    aria-controls="primary-search-account-menu"
                    aria-haspopup="true"
                    color="inherit"
                >
                    <AccountCircle/>
                </IconButton>
                <p>Profile</p>
            </MenuItem>
        </Menu>
    );


    const [drawerOpen, setDrawerOpen] = useState(false);

    const toggleDrawer = (open) => event => {
        if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }
        setDrawerOpen(open);
    };
    const sideList = () => (
        <div
            className={classes.list}
            role="presentation"
            onClick={toggleDrawer(false)}
            onKeyDown={toggleDrawer(false)}>
            <List>
                <Typography style={{marginRight : 20,marginLeft : 20}} display="block" variant='h6' gutterBottom>Choose The Language</Typography>
                <ListItem>
                    <Button variant='contained' color={isLangEnglish() || (!isLangPersian() && !isLangEnglish()) ? 'primary' : ''} style={{color : 'white'}} onClick={()=>{setLangHandler('en')}}>English</Button>
                    <Button variant='contained' color={isLangPersian() ? 'primary' : ''} style={{marginLeft : 10,color : 'white'}} onClick={()=>{setLangHandler('fa')}}>Persian</Button>
                </ListItem>
                <Divider/>
            </List>
        </div>
    );
    const chatWindow = useRef();
    const dispatch = useDispatch();

    useEffect(() => {
        ((async ()=>{
            console.log(Env.userId);
            try{
                var res = await fetchServer('/admin-panel/chats/chat/' + Env.userId, 'POST');
                var result = await res.json();
                console.log(result);
                dispatch(addAllChatMessages(result.messages));
            }catch (e) {console.log(e);}
        }))();
    }, []);


    return (
        <div className={classes.grow}>
            <ChatWindow ref={chatWindow} onSubmit={(text)=>{
                if(Ws.isAvailable()){
                    // Ws.send(currentOtherUserId.current,{'msg':text},false);
                    // chatWindow.current.clearInput();
                }
            }}  title='' subtitle={'چت با پشتیبانی'} disabled={false}>
                {chatMessages.map((item)=>{
                    return <MessageChatItem key={item.id} message={item.message} date={item.time} isMe={item.from_id === 1}/>
                })}
            </ChatWindow>
            <AppBar position="sticky" color='primary' style={{color: 'white'}}>
                <SwipeableDrawer
                    anchor="left"
                    open={drawerOpen}
                    onClose={toggleDrawer(false)}
                    onOpen={toggleDrawer(true)}>
                    {sideList()}
                </SwipeableDrawer>
                <Toolbar>
                    <IconButton
                        edge="start"
                        className={classes.menuButton}
                        color="inherit"
                        aria-label="open drawer"
                        onClick={() => {
                            setDrawerOpen(true)
                        }}
                    >
                        <MenuIcon/>
                    </IconButton>
                    <Typography className={classes.title} variant="h6" noWrap>
                        Royal Exchange
                    </Typography>
                    <div className={classes.grow}/>
                    <div className={classes.sectionDesktop}>
                        <IconButton aria-label="show 4 new mails" color="inherit">
                            <Badge badgeContent={0} color="secondary">
                                <MailIcon/>
                            </Badge>
                        </IconButton>
                        <IconButton aria-label="show 17 new notifications" color="inherit">
                            <Badge badgeContent={17} color="secondary">
                                <NotificationsIcon/>
                            </Badge>
                        </IconButton>
                        <IconButton
                            edge="end"
                            aria-label="account of current user"
                            aria-controls={menuId}
                            aria-haspopup="true"
                            onClick={handleProfileMenuOpen}
                            color="inherit"
                        >
                            <AccountCircle/>
                        </IconButton>
                    </div>
                    <div className={classes.sectionMobile}>
                        <IconButton
                            aria-label="show more"
                            aria-controls={mobileMenuId}
                            aria-haspopup="true"
                            onClick={handleMobileMenuOpen}
                            color="inherit"
                        >
                            <MoreIcon/>
                        </IconButton>
                    </div>
                </Toolbar>
            </AppBar>
            {renderMobileMenu}
            {renderMenu}
            <div style={{
                marginTop: 'auto',
                textAlign: 'center',
                backgroundImage: 'url("/img/website/header_pic.jpg")',
                alignItems: 'center',
                flex: 1,
                transform: 'translate3d(0px, 0px, 0px);',
                height: 500,
                width: '100%',
                backgroundRepeat: "initial"
            }}>
                <img className="rounded-circle" src='/img/website/logo.jpg' width={200}
                     style={{position: 'relative', top: '20%'}}/>
            </div>
            {props.payload.content}
            <Grid container justify='center' alignItems='center' className="mb-4">
                <Grid xs={12} md={3} className="text-center" item>
                    <Button color="primary">About Us</Button>
                </Grid>
                <Grid xs={12} md={6} className="text-center" item>
                    <h6 className="text-white">© 2020 , All Rights Reserved to Royal Exchange.</h6>
                </Grid>
            </Grid>
        </div>
    );
}
