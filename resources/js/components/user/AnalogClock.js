import * as React from 'react';
import PropTypes from 'prop-types';

export default function AnalogClock(props) {
    let {
        size,
        colorClock,
        colorNumber,
        colorCenter,
        colorHour,
        colorMinutes,
        hour,
        minutes,
    } = props;
    var date = new Date();
    if (!hour) hour = date.getHours();
    hour = hour > 12 ? hour - 12 : hour;
    if (!minutes) minutes = date.getMinutes();
    minutes = minutes / 5;

    var lanHour = size / 6;
    var lanMinutes = size / 3.75;
    const ml = 50;
    const mr = 35;

    return (
        <div
            style={{
                backgroundColor: colorClock,
                // borderRadius: size / 2,
                justifyContent: 'center',
                alignItems: 'center',
                borderWidth : 3,
                borderColor : 'red',
                height: size,
                width: size,
            }}>
            {[...Array(12).keys()].map(i => {
                let a = -60 + 30 * i;
                let b = 60 - 30 * i;
                return (
                    <div
                        key={i}
                        style={{
                            position : 'absolute',
                            marginLeft : ml,
                            marginTop : mr,
                            transform: `rotate(${a}deg) translate(${size / 2 - 15}px,0px)`,
                        }}>
                        {i === 11 ? (
                            <div/>
                        ):(
                            <h6
                                style={{
                                    color: colorNumber,
                                    fontSize: 10,
                                    fontWeight: 'bold',
                                    transform: `rotate(90deg)`,
                                }}>
                                |
                            </h6>
                        )}
                    </div>
                );
            })}
            <div
                style={{
                    position : 'absolute',
                    marginLeft : ml,
                    marginTop : mr,
                    zIndex: 1,
                    width: 8,
                    height: 8,
                    borderRadius: 4,
                    backgroundColor: colorCenter,
                }}
            />
            <div
                style={{
                    position: 'absolute',
                    marginLeft : ml,
                    marginTop : mr,
                    width: lanHour,
                    height: 4,
                    borderRadius: 4,
                    backgroundColor: colorHour,
                    transform: `rotate(${-90 + hour * 30}deg) translate(${lanHour / 2}px,0px)`,
                }}
            />
            <div
                style={{
                    position: 'absolute',
                    marginLeft : ml,
                    marginTop : mr,
                    width: lanMinutes,
                    height: 4,
                    borderRadius: 4,
                    backgroundColor: colorMinutes,
                    transform: `rotate(${-90 + minutes * 30}deg) translate(${lanMinutes / 2}px,0px)`,
                }}
            />
        </div>
    );
}


AnalogClock.propTypes = {
  size: PropTypes.number,
  colorClock: PropTypes.string,
  colorNumber: PropTypes.string,
  colorCenter: PropTypes.string,
  colorHour: PropTypes.string,
  colorMinutes: PropTypes.string,
};

AnalogClock.defaultProps = {
  size: 180,
  colorClock: 'rgba(255,255,255,0.8)',
  colorNumber: '#fff',
  colorCenter: '#fff',
  colorHour: '#fff',
  colorMinutes: 'rgba(255,255,255,0.7)',
};
