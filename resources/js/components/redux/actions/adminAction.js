export const ADD_NOTIFICATION = 'ADD_NOTIFICATION';
export const ADD_MESSAGE = 'ADD_MESSAGE';

export const addMessage = (title,starColor,time,picture) => {
    return{
        type: ADD_MESSAGE,
        message : {title,starColor,time,picture}
    };
};


export const addNotification = (title, link, time , icon) => {
    return{
        type: ADD_NOTIFICATION,
        notification : {title,link,time,icon}
    };
};
