import React from 'react';
import {BrowserRouter, Route} from 'react-router-dom';
import AdminLayout from "./admin/AdminLayout";
import adminReducer from "./redux/reducer/adminReducer";
import {combineReducers, createStore} from 'redux';
import {Provider} from 'react-redux';
import moment from "moment";
import NavDropDownMessageMenu from "./admin/NavDropDownMessageMenu";
import NavDropDownNotificationsMenu from "./admin/NavDropDownNotificationsMenu";

import {Env} from "../env";
import exchangeReducer from "./admin/redux/exchangeReducer";
import ExchangeUpdateAdminScreen from "./admin/screens/ExchangeUpdateAdminScreen";
import OrdersAdminScreen from "./admin/screens/OrdersAdminScreen";
import orderReducer from "./admin/redux/orderReducer";
import ChatsAdminScreen from "./admin/screens/ChatsAdminScreen";
import chatsReducer from "./admin/redux/chatsReducer";
import NewsAdminScreen from "./admin/screens/NewsAdminScreen";
import CreateNewsAdminScreen from "./admin/screens/CreateNewsAdminScreen";
import WebsocketWrapper from "./WebsocketWrapper";
import UserLayout from "./user/UserLayout";
import {createMuiTheme, MuiThemeProvider} from '@material-ui/core/styles';
import UserMainScreen from "./user/UserMainScreen";
import i18n from 'i18n-js';
import {en, fa} from "./constants/translation";
import Cookies from "universal-cookie";
import UserNewsScreen from "./user/UserNewsScreen";
import UserNewOrderScreen from "./user/UserNewOrderScreen";
import UserOrdersScreen from "./user/UserOrdersScreen";

moment.locale('fa');

const rootReducer = combineReducers({
    admin: adminReducer,
    exchange: exchangeReducer,
    orders: orderReducer,
    chats: chatsReducer
});

const theme = createMuiTheme({
        palette: {
            primary: {
                main: '#d2ac50'
            },
            secondary: {
                main: '#287727'
            }
        }
    },
);
const cookies = new Cookies();
const App = props => {
    Env.userId = props.userId;
    Env.csrf = props.csrf;
    Env.ratchetToken = props.token;

    i18n.fallbacks = true;
    i18n.translations = {en, fa};
    i18n.locale = cookies.get('lang') ? cookies.get('lang') : 'en';

    let sharedAdminProperties = {
        adminName: 'admin',
        search: false,
        dropdown: (
            <div className="container">
                <div className="row">
                    <NavDropDownMessageMenu footerLink='test'/>
                    <NavDropDownNotificationsMenu header='اعلانات'/>
                </div>
            </div>
        )
    };
    let nav = [
        {
            basename: '/',
            nav: [
                {
                    path: '/',
                    nav: true,
                    component: UserLayout,
                    payload: {
                        content: (
                            <UserMainScreen {...props}/>
                        ), ...sharedAdminProperties
                    }
                },
                {
                    path: '/news/*',
                    nav: true,
                    component: UserLayout,
                    payload: {
                        content: (
                            <UserNewsScreen {...props}/>
                        ), ...sharedAdminProperties
                    }
                },
                {
                    path: '/order/create',
                    nav: true,
                    component: UserLayout,
                    payload: {
                        content: (
                            <UserNewOrderScreen {...props}/>
                        ), ...sharedAdminProperties
                    }
                },
                {
                    path: '/orders',
                    nav: true,
                    component: UserLayout,
                    payload: {
                        content: (
                            <UserOrdersScreen {...props}/>
                        ), ...sharedAdminProperties
                    }
                },
            ]
        },
        {
            basename: '/admin-panel',
            nav: [
                {
                    path: '/',
                    nav: true,
                    component: AdminLayout,
                    payload: {
                        title: 'جدول ارز', content: (
                            <ExchangeUpdateAdminScreen {...props}/>
                        ), ...sharedAdminProperties
                    }
                },
                {
                    path: '/orders',
                    nav: true,
                    component: AdminLayout,
                    payload: {
                        title: 'سفارش ها', subtitle: 'با کلیک روی هر سفارش عکس پرداخت خود را آپلود کنید', content: (
                            <OrdersAdminScreen {...props}/>
                        ), ...sharedAdminProperties
                    }
                },
                {
                    path: '/chats',
                    nav: true,
                    component: AdminLayout,
                    payload: {
                        title: 'چت ها', content: (
                            <ChatsAdminScreen {...props}/>
                        ), ...sharedAdminProperties
                    }
                },
                {
                    path: '/news',
                    nav: true,
                    component: AdminLayout,
                    payload: {
                        title: 'اخبار', content: (
                            <NewsAdminScreen {...props}/>
                        ), ...sharedAdminProperties
                    }
                },
                {
                    path: '/news/create',
                    nav: true,
                    navIncluded: false,
                    component: AdminLayout,
                    payload: {
                        title: 'خبر جدید', content: (
                            <CreateNewsAdminScreen {...props}/>
                        ), ...sharedAdminProperties
                    }
                },
                {
                    path: '/news/update/*',
                    nav: true,
                    navIncluded: false,
                    component: AdminLayout,
                    payload: {
                        title: 'آپدیت خبر', content: (
                            <CreateNewsAdminScreen {...props}/>
                        ), ...sharedAdminProperties
                    },
                    exact: false
                },
            ]
        },
    ];
    const findNavByBasename = (basename) => {
        for (let i = 0; i < nav.length; i++) {
            let item = nav[i];
            if (item.basename == basename) return item.nav;
        }
        return [];
    };
    const handlePath = (basename, currentNav) => {
        let nav;
        let p = {...props};
        if (currentNav.navBasename) {
            if (currentNav.navBasename === 'all') {
            } else {
                nav = findNavByBasename(currentNav.navBasename);
            }
        }
        if (currentNav.nav) {
            nav = findNavByBasename(basename);
            nav = nav.filter((item) => {
                return item.navIncluded !== false
            });
        }
        if (currentNav.props === false) {
            p = {};
        }
        return React.createElement(currentNav.component, {
            basename: basename,
            payload: currentNav.payload,
            nav: nav,
            ...p
        });
    };

    return (
        <Provider store={createStore(rootReducer)}>
            <MuiThemeProvider theme={theme}>
                <WebsocketWrapper>
                    {
                        nav.filter((item) => {
                            return window.location.href.indexOf(item.basename) !== -1;
                        }).map((item) => {
                            return (
                                <BrowserRouter key={item.basename} basename={item.basename}>
                                    <div>
                                        {item.nav.map((i) => {
                                            return <Route key={item.basename + i.path} path={i.path}
                                                          exact={i.exact !== 'false'}
                                                          component={() => {
                                                              return handlePath(item.basename, i)
                                                          }}/>
                                        })}
                                    </div>
                                </BrowserRouter>
                            )
                        })
                    }
                </WebsocketWrapper>
            </MuiThemeProvider>
        </Provider>
    )
};
export default App;
