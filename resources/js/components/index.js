import React from 'react';
import ReactDOM from 'react-dom';
import App from "./App";

const bindComponent = (container, Component) => {
    const element = document.getElementById(`rct-${container}`);
    if ( element ) {
        const props = JSON.parse(element.dataset.props);
        delete element.dataset.props;

        ReactDOM.render(<Component {...props} />, element);
    }
};
const bindComponentWithExtraProps = (container, Component,extraProps) => {
    const element = document.getElementById(`rct-${container}`);
    if ( element ) {
        const props = JSON.parse(element.dataset.props);
        delete element.dataset.props;
        console.log(props);
        ReactDOM.render(<Component {...{...props,...extraProps}}/>, element);
    }
};

bindComponent('main',App);

// ReactDOM.render(
    {/*<VideoPlayer {{autoPlay : true,}} />,*/}
    // document.getElementById('test')
// );
