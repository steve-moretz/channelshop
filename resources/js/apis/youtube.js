const KEY = 'AIzaSyCBBfmKOs233Fmsi1y-3AaEJ6V33NhGQAQ';
export default class Youtube{
    baseUrl = "https://www.googleapis.com/youtube/v3";
    params = {
        part : "snippet",
        maxResult : 5,
        key : KEY,
        q : ''
    };
    constructor(params = null){
        if(params != null) this.params = params;
    }
    getVideoList(term,callback){
        this.params.q = term;
        $.get(this.baseUrl+'/search',this.params,function (data, status) {
            callback(data,status);
        });
    }
}
