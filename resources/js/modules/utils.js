import isPlainObject from "react-redux/es/utils/isPlainObject";
import useMediaQuery from "@material-ui/core/es/useMediaQuery";
import useTheme from "@material-ui/core/es/styles/useTheme";


export function useWidth() {
    const theme = useTheme();
    const keys = [...theme.breakpoints.keys].reverse();
    return (
        keys.reduce((output, key) => {
            // eslint-disable-next-line react-hooks/rules-of-hooks
            const matches = useMediaQuery(theme.breakpoints.up(key));
            return !output && matches ? key : output;
        }, null) || 'xs'
    );
}

import { useState, useEffect } from 'react';

export default function useWindowDimensions() {

    const hasWindow = typeof window !== 'undefined';

    function getWindowDimensions() {
        const width = hasWindow ? window.innerWidth : null;
        const height = hasWindow ? window.innerHeight : null;
        return {
            width,
            height,
        };
    }

    const [windowDimensions, setWindowDimensions] = useState(getWindowDimensions());

    useEffect(() => {
        if (hasWindow) {
            function handleResize() {
                setWindowDimensions(getWindowDimensions());
            }

            window.addEventListener('resize', handleResize);
            return () => window.removeEventListener('resize', handleResize);
        }
    }, [hasWindow]);

    return windowDimensions;
}

export function searchAllChildren(node, name = null, fn){
    if(Array.isArray(node)){
        // console.log('isArray');
        let arr = [];
        node.forEach(child => {
            if(Array.isArray(child)){child = [...child]}else if(isPlainObject(child)){child = {...child}}
            if(child.type && (child.type.name === name || (child.type.displayName && child.type.displayName.toString().includes(name)))){
                fn(child);
            }
            arr.push(child);
            if(child.props && child.props.children){
                child.props = {...child.props};
                child.props.children = searchAllChildren(child.props.children,name,fn);
            }
        });
        return [...arr];
    }else if(isPlainObject(node)){
        // console.log('isObject');
        node = {...node};
        node.props = {...node.props};
        if(node.type && node.type.name === name || (node.type.displayName && node.type.displayName.toString().includes(name))){
            fn(node);
        }
        if(node.props && node.props.children){
            node.props.children = searchAllChildren(node.props.children,name,fn);
        }
        return {...node};
    }else{
        // console.log('is nor');
        return node;
    }
}
