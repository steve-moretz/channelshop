import PropTypes from 'prop-types'
import React, {forwardRef, useEffect, useImperativeHandle, useRef, useState} from 'react';
import Fab from "@material-ui/core/Fab";
import ChatIcon from '@material-ui/icons/Chat';
import SendIcon from '@material-ui/icons/Send';
import Card from "@material-ui/core/Card";
import {CardActions, CardContent, CardHeader, Collapse} from "@material-ui/core";
import List from "@material-ui/core/List";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import ListItemText from "@material-ui/core/ListItemText";
import Typography from "@material-ui/core/Typography";
import ListItem from "@material-ui/core/ListItem";
import TextField from "@material-ui/core/TextField";
import IconButton from "@material-ui/core/IconButton";
import mp3File from '../../sfx/notif.mp3';
import Colors from "../components/constants/Colors";
import Badge from "@material-ui/core/Badge";
import UIfx from 'uifx'
import useWindowDimensions from "./utils";

export const MessageChatItem = (props) => {
    if (props.isMe) {
        return (
            <div style={{borderBottom: '1px solid gray', borderRightColor: 'green'}}>
                <ListItem alignItems="flex-start" className="chat-list-item" onClick={() => {
                }}>
                    <ListItemAvatar>
                        <Avatar style={{backgroundColor: Colors.primary}}/>
                    </ListItemAvatar>
                    <ListItemText
                        className="text-right"
                        primary={
                            <React.Fragment>
                                <Typography
                                    component="div"
                                    variant="subtitle2"
                                    color="textPrimary" aria-rowcount={3}>
                                    {props.message}
                                </Typography>
                                <Typography
                                    component="div"
                                    variant="caption"
                                    color="textPrimary">
                                    {props.date}
                                </Typography>
                            </React.Fragment>
                        }
                    />
                    <div>
                    </div>
                </ListItem>

            </div>
        );
    }
    return (
        <ListItem className="text-left chat-list-item mr-auto" alignItems="center" onClick={() => {
        }} style={{borderBottom: '1px solid gray', borderRightColor: 'green'}}>
            <ListItemText
                className="text-right"
                primary={
                    <React.Fragment>
                        <Typography
                            component="div"
                            variant="subtitle2"
                            color="textPrimary" aria-rowcount={3}>
                            {props.message}
                        </Typography>
                        <Typography
                            component="div"
                            variant="caption"
                            color="textPrimary">
                            {props.date}
                        </Typography>
                    </React.Fragment>
                }
            />
            <ListItemAvatar className="mr-auto">
                <Avatar className="mr-auto"
                        style={{backgroundColor: Colors.primary}}>{props.name && props.name.toString().substr(0, 1)}</Avatar>
            </ListItemAvatar>
        </ListItem>
    )
};

const beep = new UIfx(
    mp3File,
    {
        throttleMs: 100
    }
);

const ChatWindow = forwardRef((props, ref) => {
    const [expanded, setExpanded] = useState(false);
    const [text, setText] = useState('');

    const lastMessageCount = useRef(0);

    const [badge, setBadge] = useState(0);

    const list = useRef();

    const windowRef = useRef();

    const chatRef = useRef();

    useImperativeHandle(ref, () => ({
        close() {
            setExpanded(false);
            setBadge(0);
        },
        open() {
            setExpanded(true);
            setBadge(0);
        },
        clearInput() {
            setText('');
        },
        scrollToTheEnd() {
            chatRef.current.scrollTop = windowRef.current.scrollHeight - 355.6
        },
        scrollToTheStart() {
            chatRef.current.scrollTop = 0
        },
        getScrollTop() {
            return chatRef.current.scrollTop;
        },
        getScrollHeight() {
            return windowRef.current.scrollHeight - 355.6
        }
    }));

    useEffect(() => {
        if (props.onChange) {
            props.onChange(text);
        }
    }, [text]);

    useEffect(() => {
        const diff = props.children.length - lastMessageCount.current;
        if(diff === 1 && !props.children[props.children.length - 1].props.isMe && (!document.hasFocus() || (chatRef.current.scrollTop + 200 < windowRef.current.scrollHeight - 355.6))){
            setBadge(badge + diff);
            beep.play();
        }
        lastMessageCount.current = props.children.length;
        if(props.children.length === 0)return;
        if (chatRef.current) {
            chatRef.current.scrollTop = windowRef.current.scrollHeight - 355.6
        }else{
            setTimeout(()=>{
                if (chatRef.current) {
                    chatRef.current.scrollTop = windowRef.current.scrollHeight - 355.6
                }
            },500);
        }
    }, [props.children]);

    const { height, width } = useWindowDimensions();

    return (
        <div className="chat">
            <Collapse in={expanded && !props.disabled} timeout="auto">
                <Card>
                    {
                        height >= 700 && (
                            <CardHeader title={props.title} titleTypographyProps={{color : 'secondary'}} style={{backgroundColor : Colors.primary}}
                                        subheader={<span className="text-white">{props.subtitle}</span>}>
                            </CardHeader>
                        )
                    }
                    <CardContent className="chat-window" ref={chatRef}>
                        <List className="text-center w-100" ref={windowRef} style={{margin : 0,padding : 0}}>
                            {props.children}
                        </List>
                    </CardContent>
                    <CardActions className="w-100">
                        <TextField className="flex-grow-1" value={text} id="standard-basic" label="پیام" variant="outlined"
                                   onKeyPress={(ev) => {
                                       if (ev.key === 'Enter') {
                                           if (props.onSubmit && text) {
                                               props.onSubmit(text)
                                           }
                                           ev.preventDefault();
                                       }
                                   }}
                                   onChange={(e) => {
                                       setText(e.target.value)
                                   }}/>
                        <IconButton aria-label="delete" style={{backgroundColor : 'gray'}} onClick={() => {
                            if (props.onSubmit && text) {
                                props.onSubmit(text)
                            }
                        }}>
                            <SendIcon fontSize="small" color="primary"/>
                        </IconButton>
                    </CardActions>
                </Card>
            </Collapse>
            <Badge className="float-left" overlap="circle" badgeContent={badge} color="primary">
                <Fab color="secondary" disabled={props.disabled} onClick={() => {
                    setExpanded(!expanded);
                    setBadge(0);
                }}>
                    <ChatIcon/>
                </Fab>
            </Badge>
        </div>
    )
});

export default ChatWindow;

MessageChatItem.propTypes = {
    date: PropTypes.any,
    isMe: PropTypes.any,
    message: PropTypes.any,
    name: PropTypes.any
}

ChatWindow.propTypes = {
    children: PropTypes.any,
    disabled: PropTypes.any,
    onChange: PropTypes.any,
    onSubmit: PropTypes.any,
    subtitle: PropTypes.any,
    title: PropTypes.any
}
