import {Env} from "../env";
const axios = require('axios');
export default async (url,method = 'GET',body,headers = {},props) => {
    if(method !== 'POST'){
        body = undefined;
    }
    return await fetch(Env.domain +'/'+ url, {
        credentials: "same-origin",
        ...props,
        method : method,
        headers: {
            'X-CSRF-TOKEN': Env.csrf,
            'X-Requested-With': 'XMLHttpRequest',
            'Content-Type': 'application/json',
            ...headers
        },
        body: JSON.stringify({...body})
    });
}

export const axiosServer = async (url,method = 'GET') => {
    try {
        var res = await axios.post(url, {
            headers: {
                'X-CSRF-TOKEN': Env.csrf,
                'X-Requested-With': 'XMLHttpRequest',
                'Content-Type': 'application/json',
            },
            method: method,
            validateStatus: (status) => {
                return true; // I'm always returning true, you may want to do it depending on the status received
            },
        });
        console.log(res);
        var data = res.data;
        console.log(data);
    } catch (e) {
        console.log(e);
    }
}
