const showNotification = (title,body,icon,onclick,onclose) => {
    // Let's check if the browser supports notifications
    if (!("Notification" in window)) {
        alert("This browser does not support desktop notification");
    }

    // Let's check if the user is okay to get some notification
    else if (Notification.permission === "granted") {
        // If it's okay let's create a notification
        var notification = new window.Notification(title, {
            icon: icon,
            body: body,
        });
        notification.onclick = onclick;
        notification.onclose = onclose;
    }

    // Otherwise, we need to ask the user for permission
    // Note, Chrome does not implement the permission static property
    // So we have to check for NOT 'denied' instead of 'default'
    else if (Notification.permission !== 'denied') {
        window.Notification.requestPermission(function (permission) {

            // Whatever the user answers, we make sure we store the information
            if (!('permission' in Notification)) {
                Notification.permission = permission;
            }

            // If the user is okay, let's create a notification
            if (permission === "granted") {
                var notification = new window.Notification(title, {
                    icon: icon,
                    body: body,
                });
                notification.onclick = onclick;
                notification.onclose = onclose;
            } else {
                alert("You should give browser permission");
            }
        });
    } else {
        alert(`Permission is ${Notification.permission}`);
    }
};
export default showNotification ;
