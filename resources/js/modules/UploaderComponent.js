import PropTypes from 'prop-types'
import React, {forwardRef, useImperativeHandle, useRef} from 'react';
import {Env} from "../env";
import {Button} from "@material-ui/core";

const axios = require('axios');

const UploaderComponent = forwardRef((props, ref)=> {
    const file = useRef();

    useImperativeHandle(ref, () => ({
        async submit() {
            return await onChangeHandler('',true);
        }
    }));

    const onChangeHandler = async (e,upload) => {
        var item = file.current.files[0];
        if(props.onPictureSrcSelected)props.onPictureSrcSelected(URL.createObjectURL(item));
        if(props.noAutoUpload === true && !upload){return }
        if(!item)return ;
        let formData = new FormData();
        formData.append('file', item);
        for(var i in props.body){
            formData.append(i,props.body[i]);
        }
        try {
            var res = await axios.post(props.url, formData, {
                headers: {
                    'X-CSRF-TOKEN': Env.csrf,
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': 'multipart/form-data'
                },
                method: 'POST',
                onUploadProgress: (p) => {
                    if(props.onUploadPercent){
                        props.onUploadPercent(p.loaded / p.total * 100)
                    }
                },
                // validateStatus: (status) => {
                //     return true; // I'm always returning true, you may want to do it depending on the status received
                // },
            });
            // console.log(res);
            var data = res.data;
            if(props.onResponse){
                props.onResponse(data);
            }
            if(res.status === 200){
                return data;
            }
        }catch (e) {
            console.log(e);
        }
        return undefined;
    };
    return (
        <div className={props.className} style={props.style}>
            {props.children && <div onClick={()=>{file.current.click()}}>{props.children}</div>}
            {!props.children && <Button onClick={()=>{file.current.click()}}>Upload</Button>}
            <input hidden ref={file} onChange={onChangeHandler} type="file" accept={props.accept}/>
        </div>
    )
});

export default UploaderComponent;

UploaderComponent.propTypes = {
  accept: PropTypes.any,
  body: PropTypes.any.isRequired,
  children: PropTypes.any,
  className: PropTypes.any,
  noAutoUpload: PropTypes.any,
  onPictureSrcSelected: PropTypes.any,
  onResponse: PropTypes.any,
  onUploadPercent: PropTypes.any,
  style: PropTypes.any,
  url: PropTypes.any.isRequired
}
