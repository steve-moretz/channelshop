<script>
    var conn = new WebSocket('{{env('WS_URL')}}');
    conn.onerror = function(e){
        console.log('error occured');
        console.log(e);
    };
    conn.onopen = function(e) {
        console.log("Connection established!");
        conn.send(JSON.stringify({command: "auth", token : '{{$token}}'}));
        // conn.send(JSON.stringify({command: "register", userId: 9}));
        // conn.send(JSON.stringify({command: "message", from:"9", to: "1", message: "Hello"}));
    };
    conn.onmessage = function(e) {
        console.log(e.data);
    };
</script>
