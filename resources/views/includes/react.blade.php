<?php
$controllerVariables = get_defined_vars()['__data'];
unset($controllerVariables['__env'],$controllerVariables['app'],$controllerVariables['errors'],$controllerVariables['obLevel'],$controllerVariables['style'],$controllerVariables['id'],$controllerVariables['props']);
?>
<div id="rct-{{ $id }}" data-props='@json($props+$controllerVariables)' style="{{$style}}"></div>
