<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRatchetUserPoliciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ratchet_user_policies', function (Blueprint $table) {
            $table->unsignedInteger('blocker')->default(null);
            $table->unsignedInteger('blocked')->default(null);
            $table->unsignedInteger('allower')->default(null);
            $table->unsignedInteger('allowed')->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ratchet_user_policies');
    }
}
