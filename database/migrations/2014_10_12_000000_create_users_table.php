<?php

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            if (env('INCLUDE_EMAIL',true)) {
                $table->string('email')->unique();
            }
            if (env('INCLUDE_PHONE',true)) {
                $table->string('phone')->unique();
            }
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->string('activation_token')->nullable();
            $table->boolean('checked')->default(true);
            $table->softDeletes();
            $table->timestamps();
            if (env('REGISTER_VIA_PHONE',false)) {
                $table->string('phone');
            }
            $table->timestamp('subscription_expiration')->nullable();
            $table->integer('subscription_length')->nullable();
        });
        if(User::count() === 0){
            $user = new User();
            $user->email = env('ADMIN_EMAIL','admin@gmail.com');
            $user->name = 'admin';
            $user->email_verified_at = Carbon::now();
            if (env('REGISTER_VIA_PHONE',false)) {
                $user->phone = env('ADMIN_PHONE_IF_REGISTER_VIA_PHONE','+98911111111');
            }
            if(env('INCLUDE_PHONE')){
                $user->phone = env('ADMIN_PHONE_IF_REGISTER_VIA_PHONE','+98911111111');
            }
            $user->password = \Hash::make(env('ADMIN_PASSWORD','12345678'));
            $user->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
